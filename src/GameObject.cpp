#include "GameObject.hpp"

#include "helper.hpp"

#include <Box2D/Box2D.h>

jngl::Vec2 GameObject::getPosition() const {
	//return meterToPixel(body->GetPosition());
	return { body->GetPosition().x, body->GetPosition().y };
}

void GameObject::setPosition(const jngl::Vec2 position) {
	const auto transform = body->GetTransform();
	body->SetTransform(v2v(position), transform.q.GetAngle());
}

int GameObject::getAmount() const {
	return 0;
}

double GameObject::getZIndex() const {
	return getPosition().y;
}

void GameObject::onContact(GameObject* other) {
}

void GameObject::createFixtureFromShape(const b2Shape& shape) {
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	//fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
}
