#pragma once

#include <string>

const std::string programDisplayName = "Pommes Schranke";

constexpr int BOUNDS_W = 960;
constexpr int BOUNDS_H = 540;

void printCentered(const std::string& text, double x, double y);
