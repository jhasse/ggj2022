#pragma once

#include <Box2D/Box2D.h>
#include <jngl.hpp>

b2Vec2 v2v(jngl::Vec2);
jngl::Vec2 v2v(b2Vec2);
