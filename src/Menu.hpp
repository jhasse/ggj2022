#pragma once

#include "Game.hpp"
#include "gui/Button.hpp"

class Menu : public jngl::Work {
public:
	explicit Menu(const std::shared_ptr<jngl::Work>& game);
	~Menu() override;

	void step() override;
	void draw() const override;

	void onQuitEvent() override;

private:
	std::shared_ptr<Game> game;
	jngl::FrameBuffer fbo;
	jngl::FrameBuffer fbo2;

	jngl::Container container;

	jngl::Shader fragment;
	jngl::ShaderProgram blur;

	Button music;
	Button sound;

	constexpr static double REDUCE_RESOLUTION = 4;
};
