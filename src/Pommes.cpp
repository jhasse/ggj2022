#include "Pommes.hpp"

#include "helper.hpp"

Pommes::Pommes(b2World& world, const jngl::Vec2 position, const jngl::Vec2 size,
               const jngl::Vec2 speed)
: world(world), size(size), speed(speed) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position = b2Vec2(position.x,position.y);
	body = world.CreateBody(&bodyDef);
	// body->SetLinearDamping(10.f);
	body->SetAngularDamping(10);

	rotation=rand();
	//body->GetUserData() = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2-size.x*0.05, size.y / 2-size.y*0.05, b2Vec2(0, 0), 0);
	createFixtureFromShape(shape);

	body->SetTransform(v2v(getPosition()), rotation);
}

Pommes::~Pommes() {
	world.DestroyBody(body);
}

bool Pommes::step() {
	bool deleteMe = checkOutOfScreen();
	setPosition(getPosition() + speed);
	return deleteMe;
}

bool Pommes::checkOutOfScreen() {
	jngl::Vec2 pos = getPosition();
	jngl::Vec2 size = getSize();
	float width = jngl::getScreenWidth() / 2.f;
	float height = jngl::getScreenHeight() / 2.f;

	// ueber den Bildschirmrand
	if (pos.x + size.x < -width) {
		setPosition({ width + size.x / 2, pos.y });
		return true;
	}
	if (pos.x - size.x > width) {
		setPosition({ -width - size.x / 2, pos.y });
		return true;
	}
	if (pos.y + size.y > height) {
		setPosition({ pos.x, -height });
	}
	if (pos.y - size.y < -height) {
		setPosition({ pos.x, height });
	}
	return false;
}

void Pommes::draw() const {


	const auto transform = body->GetTransform();
	sprite.draw(jngl::modelview()
	                .translate(v2v(transform.p))
	                .rotate(transform.q.GetAngle() + M_PI / 2)
	                .scale(size.y / 160, size.x / 770));
	// jngl::pushMatrix();
	//jngl::translate(jngl::Vec2(transform.p.x, transform.p.y));
	//jngl::rotate(transform.q.GetAngle() * 180 / M_PI);
	//jngl::rotate(transform.q.GetAngle() * 180 / M_PI);
	//jngl::scale(size.x/770*2,size.y/160*2);
	//sprite.draw();
	//jngl::setColor(0xFFD700_rgb);
	//jngl::drawRect(jngl::Vec2(-size.x/2,-size.y/2),jngl::Vec2(size.x,size.y));

	//jngl::rotate(transform.q.GetAngle() * 180 / M_PI);

	// jngl::translate(jngl::Vec2(transform.p.x, transform.p.y));
	//jngl::rotate(90);
	// jngl::translate(jngl::Vec2(-transform.p.x, -transform.p.y));


	//jngl::scale(size.x/770,size.y/160);
	//sprite.draw();
	//jngl::setColor(0xFFD700_rgb);
	//jngl::drawRect(jngl::Vec2(-size.x/2,-size.y/2),jngl::Vec2(size.x,size.y));
	//jngl::popMatrix();

	//jngl::pushMatrix();
	// jngl::translate(jngl::Vec2(transform.p.x, transform.p.y));
	// jngl::rotate(transform.q.GetAngle() * 180 / M_PI);
	//jngl::rotate(transform.q.GetAngle() * 180 / M_PI);
	//jngl::scale(0.15,0.15);
	//sprite.draw();
	//  jngl::setColor(0xFFD700_rgb,100);
	//  jngl::drawRect(jngl::Vec2(-size.x/2,-size.y/2),jngl::Vec2(size.x-size.x*0.1,size.y-size.y*0.1));
	//  jngl::popMatrix();


}

float Pommes::getSpeedDirection() const {
	return speed.x > 0 ? 1 : -1;
}

jngl::Vec2 Pommes::getSize() const {
	return jngl::Vec2(200, 200);
}
/*
void Pommes::onContact(GameObject* other){
	if(const auto punch = dynamic_cast<PunchAction*>(other)) {
		b2Vec2 vec = body->GetPosition() - pixelToMeter(punch->getPosition());
		body->ApplyLinearImpulse(vec, body->GetPosition(), true);
	}
}
*/
