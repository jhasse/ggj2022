#include "helper.hpp"

b2Vec2 v2v(jngl::Vec2 v) {
	return { static_cast<float32>(v.x), static_cast<float32>(v.y) };
}

jngl::Vec2 v2v(b2Vec2 v) {
	return { v.x, v.y };
}
