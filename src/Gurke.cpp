#include "Gurke.hpp"

#include "helper.hpp"

Gurke::Gurke(b2World& world, const jngl::Vec2 position, const float size) : size(size) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = v2v(position);
	body = world.CreateBody(&bodyDef);
	// body->SetLinearDamping(10.f);
	body->SetAngularDamping(1.5);

	//body->GetUserData() = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));

	b2CircleShape shape;
	shape.m_radius = size;
	createFixtureFromShape(shape);
}

bool Gurke::step() {
	return getPosition().y < -jngl::getScreenHeight();
}

void Gurke::draw() const {
	const auto transform = body->GetTransform();
	sprite.draw(jngl::modelview().translate(v2v(transform.p)).rotate(transform.q.GetAngle()));
}
/*
void Pommes::onContact(GameObject* other){
	if(const auto punch = dynamic_cast<PunchAction*>(other)) {
		b2Vec2 vec = body->GetPosition() - pixelToMeter(punch->getPosition());
		body->ApplyLinearImpulse(vec, body->GetPosition(), true);
	}
}
*/
