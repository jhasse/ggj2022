#pragma once

#include "GameObject.hpp"

#include <Box2D/Box2D.h>
#include <jngl.hpp>

class Burger : public GameObject {
public:
	Burger(b2World&, bool wantsKetchup, bool wantsMayo, float speed);
	~Burger() override;
	bool step() override;
	void draw() const override;
	bool wantsKetchup;
	bool wantsMayo;

private:
	b2World& world;
	jngl::Sprite sprite;
	jngl::Sprite teller;
	float speed;
};
