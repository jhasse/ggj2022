#pragma once

#include "GameObject.hpp"

#include <Box2D/Box2D.h>
#include <jngl.hpp>

class Control;

class Tube : public GameObject {
public:
	Tube(b2World&, b2ParticleSystem&, jngl::Vec2 position, std::unique_ptr<Control>, bool majo);
	~Tube() override;
	bool step() override;
	void draw() const override;
	bool isSqueezing() const;

private:
	float rotation = 0;
	bool majo;
	std::unique_ptr<Control> control;
	jngl::Sprite sprite;
	b2ParticleSystem& particleSystem;
	int stepsSinceLastSpawn = 0;
	bool squeezing = false;
};
