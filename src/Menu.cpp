#include "Menu.hpp"

#include "engine/Fade.hpp"
#include "Game.hpp"
#include "Options.hpp"

#include <fstream>
#include <fmt/format.h>

void replaceAll(std::string& subject, const std::string& search, const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}

std::stringstream loadAndReplace(int width, int height) {
	std::ifstream source("blur.frag");
	std::stringstream buffer;
	buffer.exceptions(std::ios_base::failbit);
	buffer << source.rdbuf();
	std::string tmp = buffer.str();
	replaceAll(tmp, "FBO_WIDTH", std::to_string(width) + ".f");
	replaceAll(tmp, "FBO_HEIGHT", std::to_string(height) + ".f");
	return std::stringstream(tmp);
}

Menu::Menu(const std::shared_ptr<jngl::Work>& game)
: game(std::dynamic_pointer_cast<Game>(game)),
  fbo(jngl::getWindowSize()[0] / REDUCE_RESOLUTION, jngl::getWindowSize()[1] / REDUCE_RESOLUTION),
  fbo2(jngl::getWindowSize()[0] / REDUCE_RESOLUTION, jngl::getWindowSize()[1] / REDUCE_RESOLUTION),
  fragment(loadAndReplace(jngl::getWindowWidth() / REDUCE_RESOLUTION,
                          jngl::getWindowHeight() / REDUCE_RESOLUTION),
           jngl::Shader::Type::FRAGMENT),
  blur(jngl::Sprite::vertexShader(), fragment),
  music(
      fmt::format("Music: {}", Options::handle().music ? "On" : "Off"), jngl::Vec2(0, 0),
      [this]() {
	      if ((Options::handle().music = !Options::handle().music)) {
		      jngl::loop("sfx/Groove.ogg");
	      } else {
		      jngl::stop("sfx/Groove.ogg");
	      }
	      music.setLabel(fmt::format("Music: {}", Options::handle().music ? "On" : "Off"));
      },
      true),
  sound(
      fmt::format("Sound: {}", Options::handle().sound ? "On" : "Off"), jngl::Vec2(0, 90),
      [this]() {
	      Options::handle().sound = !Options::handle().sound;
	      sound.setLabel(fmt::format("Sound: {}", Options::handle().sound ? "On" : "Off"));
      },
      true) {
	if (!this->game->hasEnded()) {
		container.addWidget<Button>(
		    "Resume", jngl::Vec2(0, -180), [this]() { onQuitEvent(); }, true);
	}
	container.addWidget<Button>(
	    "Restart", jngl::Vec2(0, -90), []() { jngl::setWork<Fade>(std::make_shared<Game>()); },
	    true);
	if (jngl::canQuit()) {
		container.addWidget<Button>("Quit", jngl::Vec2(0, 180), []() { jngl::quit(); }, true);
	}
	jngl::setMouseVisible(true);
}

Menu::~Menu() {
	jngl::setMouseVisible(false);
	Options::handle().save();
}

void Menu::step() {
	container.step();
	music.step();
	sound.step();
	if (jngl::keyPressed(jngl::key::Escape)) {
		onQuitEvent();
	}
}

void Menu::draw() const {
	{
		auto context = fbo.use();
		jngl::scale(1. / REDUCE_RESOLUTION);
		game->draw();
	}
	{
		auto context = fbo2.use();
		fbo.draw(jngl::modelview(), &blur);
	}
	fbo2.draw(jngl::modelview().scale(REDUCE_RESOLUTION));
	container.draw();
	music.draw();
	sound.draw();

	if (!game->hasEnded()) {
		jngl::setFontColor(0x333333_rgb);
		jngl::setFontSize(50);
		std::string tmp = "Highscore: " + std::to_string(Options::handle().highscore);
		jngl::print(tmp, -jngl::getTextWidth(tmp) / 2, 385);
	}
}

void Menu::onQuitEvent() {
	jngl::setWork(this->game);
}
