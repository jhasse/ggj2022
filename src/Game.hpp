#pragma once

#include "ProductionLine.hpp"

class GameObject;
class Tube;

class Game : public jngl::Work, public std::enable_shared_from_this<Game> {
public:
	Game();
	void step() override;
	void draw() const override;
	bool hasEnded() const;

private:
	b2World world;
	ProductionLine productionLine;

	std::vector<std::shared_ptr<GameObject>> gameObjects;

	b2ParticleSystem* particleSystem = nullptr;
	b2ParticleGroup* majo = nullptr;
	b2ParticleGroup* ketchup = nullptr;

	jngl::FrameBuffer majoFb;
	mutable std::unique_ptr<jngl::Shader> fragmentShader;
	mutable std::unique_ptr<jngl::ShaderProgram> shaderProgram;

	std::shared_ptr<Tube> tube1, tube2;
	int currentSqueezeSound = 1;
	bool wasSqueezeSoundPlaying = false;
	bool gameOver = false;
};
