#pragma once

#include "Burger.hpp"
#include "Gurke.hpp"

/// Das Fließband unten auf dem die Burger laufen
class ProductionLine {
public:
	explicit ProductionLine(b2World&);

	/// Wenn true zurück gibt, soll gezählt werden
	bool step();
	void draw() const;
	bool lastBurgerSuccessfullyCompleted();
	bool lost() const;
	int getPoints() const;

private:
	b2World& world;
	std::vector<std::unique_ptr<Burger>> burgers;
	std::vector<std::unique_ptr<Gurke>> gurken;
	bool burgerCompleted = false;

	int nextBurgerCountdown = 20 * 60;

	void checkBurgerCompletion(const Burger* burger);

	int points = 0;
	int multiplier = 1;
	int lastPatty = 0;
	float lastPattyFadeout = 0.f;
	int lifes = 3;
	float speed = 0.f;
};
