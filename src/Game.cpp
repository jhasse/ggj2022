#include "Game.hpp"

#include "engine/Fade.hpp"
#include "Control.hpp"
#include "Menu.hpp"
#include "Options.hpp"
#include "Pommes.hpp"
#include "Tube.hpp"

#include <cmath>

Game::Game()
: world(/* gravity: */ b2Vec2(0, 100.0)), productionLine(world), majoFb(jngl::getWindowSize()) {
	jngl::setBackgroundColor(jngl::Color(0, 0, 0));
	jngl::setMouseVisible(false);

	fragmentShader = std::make_unique<jngl::Shader>(
	    jngl::readAsset("liquid.frag").str().c_str(), jngl::Shader::Type::FRAGMENT,
	    jngl::readAsset("liquid.gles20.frag").str().c_str());
	shaderProgram =
	    std::make_unique<jngl::ShaderProgram>(jngl::Sprite::vertexShader(), *fragmentShader);

	b2ParticleSystemDef particleSystemDef;
	particleSystemDef.dampingStrength = 1.0;
	particleSystemDef.ejectionStrength = 0.1;
	particleSystemDef.pressureStrength = 0.1;
	particleSystemDef.repulsiveStrength = -0.2;
	particleSystemDef.springStrength = 0.0;
	particleSystemDef.surfaceTensionNormalStrength = 0.75;
	particleSystemDef.surfaceTensionPressureStrength = 0.75;
	particleSystemDef.viscousStrength = 1.0f;
	particleSystemDef.colorMixingStrength = 0.1;
	particleSystemDef.radius = 3.f;
	particleSystem = world.CreateParticleSystem(&particleSystemDef);

	particleSystem->SetGravityScale(1.0f);
	particleSystem->SetDensity(1.2f);

	int width = jngl::getScreenWidth() ;
	for(int i=0;i<(15 + ( std::rand() % ( 25 ) ));i++){
		gameObjects.emplace_back(
		    std::make_shared<Pommes>(
		        world,
		        jngl::Vec2((-width/2 + ( std::rand() % ( width ) )), (-90) + ( std::rand() % ( 280 ) )),
		        jngl::Vec2((80 + ( std::rand() % ( 200 ) )), (20 + ( std::rand() % ( 20 ) ))),
		        jngl::Vec2((i % 2 ? -1 : 1) * (50 + rand() % 100) / 100.f, 0)
		        )
		    );
	}
	/*
	gameObjects.emplace_back(
	    std::make_shared<Pommes>(world, jngl::Vec2(-100, -20), jngl::Vec2(170, 28),0));
	gameObjects.emplace_back(
	    std::make_shared<Pommes>(world, jngl::Vec2(100, 20), jngl::Vec2(175, 28),1));
		*/
	gameObjects.emplace_back(
	    tube1 = std::make_shared<Tube>(world, *particleSystem, jngl::Vec2(-280, -430),
	                                   std::make_unique<KetchupControl>(), true));
	gameObjects.emplace_back(
	    tube2 = std::make_shared<Tube>(world, *particleSystem, jngl::Vec2(280, -430),
	                                   std::make_unique<MajoControl>(), false));
	if (Options::handle().music) {
		jngl::loop("sfx/Groove.ogg");
	}

	/// Einmal Box2D steppen um die Gurke aus dem Patty zu kriegen:
	world.Step(1.f / static_cast<float>(jngl::getStepsPerSecond()), 8, 3, 2);
}

void Game::step() {
	world.Step(1.f / float(jngl::getStepsPerSecond()), 8, 3, 2);

	bool burgerCompleted = productionLine.step();

	b2Vec2* positions = particleSystem->GetPositionBuffer();
	for (int32_t i = 0; i < particleSystem->GetParticleCount(); ++i) {
		const auto pos = positions[i];
		if (abs(pos.x) > jngl::getScreenWidth() || abs(pos.y) > jngl::getScreenHeight()) {
			particleSystem->SetParticleFlags(i, b2_zombieParticle); // Delete particle
		}
	}
	if (burgerCompleted) {
		if (productionLine.lastBurgerSuccessfullyCompleted()) {
			jngl::debugLn("Burger erfolgreich abgegeben.");
		} else {
			jngl::debugLn("Burger vermasselt.");
		}
	}

	for (auto it = gameObjects.begin(); it != gameObjects.end();) {
		if ((*it)->step()) {
			if (auto pommes = dynamic_cast<Pommes*>(it->get())) {
				// eine neue Pommes erstellen, damit Größe und Rotation wieder zufällig sind
				*it = std::make_shared<Pommes>(
				    world, pommes->getPosition(),
				    jngl::Vec2((80 + (std::rand() % (200))), (20 + (std::rand() % (20)))),
				    jngl::Vec2(pommes->getSpeedDirection() * (50 + rand() % 100) / 100.f, 0));
				++it;
			} else {
				it = gameObjects.erase(it);
			}
		} else {
			++it;
		}
	}

	if (Options::handle().sound) {
		if (tube1->isSqueezing() || tube2->isSqueezing()) {
			if (!wasSqueezeSoundPlaying) {
				jngl::play("sfx/squeeze" + std::to_string(currentSqueezeSound) + ".ogg");
			} else if (!jngl::isPlaying("sfx/squeeze" + std::to_string(currentSqueezeSound) +
			                            ".ogg")) {
				currentSqueezeSound += rand() % 3;
				if (currentSqueezeSound > 4) {
					currentSqueezeSound = 1;
				}
				jngl::play("sfx/squeeze" + std::to_string(currentSqueezeSound) + ".ogg");
			}
			wasSqueezeSoundPlaying = true;
		} else if (wasSqueezeSoundPlaying) {
			wasSqueezeSoundPlaying = false;
			jngl::stop("sfx/squeeze" + std::to_string(currentSqueezeSound) + ".ogg");
		}
	}
	if (jngl::keyPressed(jngl::key::Escape) || jngl::keyPressed(jngl::key::F10) || jngl::keyPressed('p')) {
		jngl::setWork<Menu>(shared_from_this());
	}
	if (!gameOver && productionLine.lost()) {
		gameOver = true;
		if (productionLine.getPoints() > Options::handle().highscore) {
			Options::handle().highscore = productionLine.getPoints();
			Options::handle().save();
		}
	}
	if (jngl::keyPressed(jngl::key::Return) && gameOver) {
		jngl::setWork<Fade>(std::make_shared<Game>());
	}
}

void Game::draw() const {
	jngl::draw("bg2", jngl::getScreenSize() / -2);
	jngl::draw("tresen", jngl::Vec2(jngl::getScreenSize().x / -2, 280));
	productionLine.draw();
	{
		auto context = majoFb.use();
		context.clear();
		b2Vec2* positions = particleSystem->GetPositionBuffer();
		b2Vec2* velocities = particleSystem->GetVelocityBuffer();
		for (int32_t i = 0; i < particleSystem->GetParticleCount(); ++i) {
			auto color = particleSystem->GetColorBuffer()[i];
			jngl::setSpriteColor(color.r, color.g, color.b);
			jngl::draw("dot", jngl::Vec2(positions[i].x, positions[i].y) - jngl::Vec2(12, 12));
		}
	}
	jngl::setSpriteColor(255, 255, 255);
	majoFb.draw(jngl::getScreenSize() / -2, &*shaderProgram);

	for (const auto& gameObject : gameObjects) {
		gameObject->draw();
	}

	if (productionLine.lost()) {
		jngl::setFontSize(80);
		jngl::setFontColor(0xff0000_rgb);
		jngl::print("YOU'RE FIRED", -300, -30);
		jngl::setFontColor(0x333333_rgb);
		jngl::setFontSize(50);
		std::string tmp = "Highscore: " + std::to_string(Options::handle().highscore);
		jngl::print(tmp, -jngl::getTextWidth(tmp) / 2, 385);
	}
}

bool Game::hasEnded() const {
	return productionLine.lost();
}
