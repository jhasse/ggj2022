#include "ProductionLine.hpp"

#include "Options.hpp"

#include <memory>

ProductionLine::ProductionLine(b2World& world) : world(world) {
	burgers.emplace_back(std::make_unique<Burger>(world, true, false, 50 + speed));
	burgers.back()->setPosition(jngl::Vec2(-600, 400));
	gurken.emplace_back(std::make_unique<Gurke>(world, jngl::Vec2(-590, 380), 45));

	burgers.emplace_back(std::make_unique<Burger>(world, true, true, 50 + speed));
	burgers.back()->setPosition(jngl::Vec2(-1500, 400));
	gurken.emplace_back(std::make_unique<Gurke>(world, jngl::Vec2(-1540, 380), 45));
}

bool ProductionLine::step() {
	if (lost()) {
		return false;
	}

	speed += 0.001f;
	lastPattyFadeout *= 0.95f;

	bool burgerRemoved = false;
	for (auto it = burgers.begin(); it != burgers.end();) {
		if ((*it)->step()) {
			checkBurgerCompletion(it->get());
			it = burgers.erase(it);
			burgerRemoved = true;
		} else {
			++it;
		}
	}
	for (auto it = gurken.begin(); it != gurken.end();) {
		if ((*it)->step()) {
			it = gurken.erase(it);
		} else {
			++it;
		}
	}
	if (--nextBurgerCountdown < 0) {
		nextBurgerCountdown = 650 + rand() % 500 - speed * 10;

		bool wantsKetchup = false;
		bool wantsMayo = false;
		int chance = rand() % 10;
		if (chance < 3) {
			wantsKetchup = true;
		} else if (chance < 6) {
			wantsMayo = true;
		} else if (chance < 9) {
			wantsKetchup = true;
			wantsMayo = true;
		}
		burgers.emplace_back(std::make_unique<Burger>(world, wantsKetchup, wantsMayo, 50 + speed));
		burgers.back()->setPosition(jngl::Vec2(-1400, 400));
		gurken.emplace_back(std::make_unique<Gurke>(world, jngl::Vec2(-1350, 380), 45));
	}
	return burgerRemoved;
}

void ProductionLine::draw() const {
	for (const auto& burger : burgers) {
		burger->draw();
	}
	for (const auto& gurke : gurken) {
		gurke->draw();
	}

	for (int i = 0; i < lifes; i++)
	{
		jngl::setFontColor(0x72bfd1_rgb);
		jngl::setFontSize(29);
		jngl::draw("burgerfull_single", -150 + 100 * i, -500);
	}

	jngl::setFontColor(0x72bfd1_rgb);
	jngl::setFontSize(29);
	jngl::print("Points:", -850, -170);
	jngl::setFontSize(42);
	jngl::print(std::to_string(points), -510 - jngl::getTextWidth(std::to_string(points)), -180);
	jngl::setFontSize(29);
	jngl::print("Multiplier:", -850, -100);
	jngl::setFontSize(42);
	jngl::print("x" + std::to_string(multiplier),
	            -510 - jngl::getTextWidth("x" + std::to_string(multiplier)), -110);
	if(lastPatty != 0)
	{
		if (lastPatty > 0)
		{
			jngl::setFontColor(0x00ff00_rgb, lastPattyFadeout);
			jngl::print("+" + std::to_string(lastPatty),
			            900 - jngl::getTextWidth(std::to_string(lastPatty)), 230);
		}else{
			jngl::setFontColor(0xff0000_rgb, lastPattyFadeout);
			jngl::print("-", 740, 230);
			jngl::pushSpriteAlpha(std::min(255.f, lastPattyFadeout * 255));
			jngl::draw("burgerfull_single", 780, 210);
			jngl::popSpriteAlpha();
		}
	}
}

void ProductionLine::checkBurgerCompletion(const Burger* burger) {
	float mayoParticles = 0;
	float ketchupParticles = 0;

	const auto particleSystem = world.GetParticleSystemList();
	b2Vec2* positions = particleSystem->GetPositionBuffer();
	for (int32_t i = 0; i < particleSystem->GetParticleCount(); ++i) {
		const auto pos = positions[i];
		if (pos.x > jngl::getScreenWidth() / 2 && 100 < pos.y && pos.y < 500) {
			auto color = particleSystem->GetColorBuffer()[i];
			float mayoFactor = (float(color.g) - 16.f) / 229.f;
			if (mayoFactor > 0.1f && mayoFactor < 0.9f) {
				ketchupParticles += 1 - mayoFactor;
				mayoParticles += mayoFactor;
			} else if (mayoFactor < 0.5f) {
				ketchupParticles += 1;
			} else {
				mayoParticles += 1;
			}
			particleSystem->SetParticleFlags(i, b2_zombieParticle);
		}
	}
	bool gurkeDabei = false;
	for (const auto& gurke:gurken){
		if (gurke->getPosition().x > jngl::getScreenWidth() / 2){
			gurkeDabei = true;
			break;
		}
	}
	jngl::debug(mayoParticles);
	jngl::debug(" majo partikel wurden abgegeben. Erforderlich: ");
	jngl::debugLn(burger->wantsMayo);
	jngl::debug(ketchupParticles);
	jngl::debug(" ketchup partikel wurden abgegeben. Erforderlich: ");
	jngl::debugLn(burger->wantsKetchup);

	float allParticles =
	    ketchupParticles + mayoParticles + 0.1f /* +0.1 um 0 division zu vermeiden */;
	float isMayoFactor = mayoParticles / allParticles;
	jngl::debug("isMayoFactor: ");
	jngl::debugLn(isMayoFactor);
	bool ketchupOk = (!burger->wantsKetchup && (allParticles < 20 || isMayoFactor > 0.9f)) ||
	                 (burger->wantsKetchup && allParticles > 20 && isMayoFactor < 0.1f);
	bool mayoOk = (!burger->wantsMayo && (allParticles < 20 || isMayoFactor < 0.1f)) ||
	              (burger->wantsMayo && allParticles > 20 && isMayoFactor > 0.9f);

	burgerCompleted =
	    (ketchupOk && mayoOk) || (burger->wantsKetchup && burger->wantsMayo && allParticles > 50 &&
	                              isMayoFactor > 0.25 && isMayoFactor < 0.75);
	int gurkenmulti = 1;
	int tmp_points = points;
	if(burgerCompleted)
	{
		if(gurkeDabei){
			gurkenmulti = 2;
		}
		if(burger->wantsKetchup && !burger->wantsMayo)  // Only Red
		{
			points += (ketchupParticles - mayoParticles) * multiplier * gurkenmulti;
		}else if (!burger->wantsKetchup && burger->wantsMayo)  // Only White
		{
			points += (mayoParticles - ketchupParticles) * multiplier * gurkenmulti;
		} else if (burger->wantsKetchup && burger->wantsMayo) // Both
		{
			auto min = fminf(mayoParticles, ketchupParticles);
			points += min * multiplier * gurkenmulti;
		} else if (!burger->wantsKetchup && !burger->wantsMayo) // None
		{
			points += (100 - mayoParticles - ketchupParticles) * multiplier * gurkenmulti;
		}
		multiplier += 1;
		if (Options::handle().sound) {
			jngl::play("sfx/success.ogg");
		}
	} else {
		if (Options::handle().sound) {
			jngl::play("sfx/wrong.ogg");
		}
		points -= 100;
		multiplier = 1;
		lifes -= 1;
	}

	lastPatty = points - tmp_points;
	lastPattyFadeout = 255.f;
	jngl::debugLn(points);
}

bool ProductionLine::lastBurgerSuccessfullyCompleted() {
	return burgerCompleted;
}

bool ProductionLine::lost() const {
	return lifes <= 0;
}

int ProductionLine::getPoints() const {
	return points;
}
