#pragma once

#include "GameObject.hpp"

#include <Box2D/Box2D.h>
#include <jngl.hpp>

class Pommes : public GameObject {
public:
	Pommes(b2World&, jngl::Vec2 position, jngl::Vec2 size, jngl::Vec2 speed);
	~Pommes() override;
	bool step() override;
	void draw() const override;

	/// returns -1 or 1
	float getSpeedDirection() const;

	//void onContact(GameObject*) override;

private:
	jngl::Vec2 getSize() const;
	bool checkOutOfScreen();

	b2World& world;
	jngl::Sprite sprite{"pom"};
    jngl::Vec2 size;
	int rotation;
	const jngl::Vec2 speed;
};
