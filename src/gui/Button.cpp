#include "Button.hpp"

Button::Button(const std::string& label, jngl::Vec2 position, std::function<void()> onClicked,
               const bool bigFont)
: jngl::Widget(position), onClicked(std::move(onClicked)),
  label((bigFont ? Button::bigFont() : font()), label) {
}

Button::Action Button::step() {
	const auto mouse = jngl::getMousePos() + label.getSize() / 2.;
	if ((mouseover = (position.x <= mouse.x && mouse.x < position.x + label.getWidth() &&
	                  position.y <= mouse.y && mouse.y < position.y + label.getHeight()))) {
		if (jngl::mousePressed()) {
			jngl::setMousePressed(jngl::mouse::Left, false);
			onClicked();
		}
	}
	return Widget::step();
}

void Button::drawSelf() const {
	jngl::setFontColor(mouseover ? 0xffffff_rgb : 0x72bfd1_rgb);
	label.draw();
}

void Button::setLabel(const std::string& text) {
	label = jngl::TextLine(bigFont(), text);
}

jngl::Font& Button::font() {
	static jngl::Font f{ "Poppins-Bold.ttf", 8 };
	return f;
}

jngl::Font& Button::bigFont() {
	static jngl::Font f{ "Poppins-Bold.ttf", 50 };
	return f;
}
