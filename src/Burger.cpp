#include "Burger.hpp"

Burger::Burger(b2World& world, bool wantsKetchup, bool wantsMayo, float speed)
: wantsKetchup(wantsKetchup), wantsMayo(wantsMayo), world(world), sprite("burger"),
  teller("teller"), speed(speed) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_kinematicBody;
	bodyDef.position = b2Vec2(0, 0);
	bodyDef.gravityScale = -1;
	body = world.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(140, 1, b2Vec2(0, 0), 0);
	createFixtureFromShape(shape);
}

Burger::~Burger() {
	world.DestroyBody(body);
}

bool Burger::step() {
	body->SetLinearVelocity(b2Vec2(speed, 0));
	return getPosition().x > 1200;
}

void Burger::draw() const {
	const auto transform = body->GetTransform();
	teller.draw(jngl::modelview().translate(getPosition() + jngl::Vec2(40, 15)).rotate(transform.q.GetAngle()));
	sprite.draw(jngl::modelview().translate(getPosition()).rotate(transform.q.GetAngle()));

	if(wantsKetchup || wantsMayo) {
		std::string flag("bothFlag");
		if (!wantsMayo) {
			flag = "ketchupFlag";
		} else if (!wantsKetchup) {
			flag = "mayoFlag";
		}
		jngl::draw(flag, getPosition() + jngl::Vec2(80, -140));
	}
}
