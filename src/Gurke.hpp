#pragma once

#include "GameObject.hpp"

#include <Box2D/Box2D.h>
#include <jngl.hpp>

class Gurke : public GameObject {
public:
	Gurke(b2World&, jngl::Vec2 position,float size);
	bool step() override;
	void draw() const override;

	//void onContact(GameObject*) override;

private:
	jngl::Sprite sprite{ "gurke1" };
	float size;
};
