#include "Tube.hpp"

#include "Control.hpp"
#include "helper.hpp"

Tube::Tube(b2World& world, b2ParticleSystem& particleSystem, jngl::Vec2 position,
           std::unique_ptr<Control> control, bool majo)
: majo(majo), control(std::move(control)), sprite(majo ? "mayotube" : "ketchuptube2"),
  particleSystem(particleSystem) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position = v2v(position);
	body = world.CreateBody(&bodyDef);

	// noch kein shape für die tuben weil noch nicht klar wie sie funktionieren sollen
}

Tube::~Tube() = default;

bool Tube::step() {
	squeezing = control->squeeze();
	if (squeezing && stepsSinceLastSpawn > 1) {
		stepsSinceLastSpawn = 0;
		b2PolygonShape shape;
		b2ParticleGroupDef pd;
		pd.shape = &shape;
		pd.stride = 0;
		{
			jngl::Mat3 tmp;
			tmp.rotate(rotation * M_PI / 180);
			boost::qvm::vec<double, 3> v{ 0, 260, 1 };
			auto x = tmp * v;
			pd.linearVelocity = b2Vec2(x.a[0], x.a[1]);
		}
		pd.flags =
		    b2_viscousParticle | b2_tensileParticle | b2_reactiveParticle | b2_colorMixingParticle;

		auto transform = body->GetTransform();

		auto pos = jngl::Vec2(-3, 280);
		jngl::Mat3 m;
		m *= boost::qvm::translation_mat(
		    boost::qvm::vec<double, 2>{ { transform.p.x, transform.p.y } });
		m.rotate(rotation * M_PI / 180.0);
		boost::qvm::vec<double, 3> v{ pos.x, pos.y, 1 };
		auto x = m * v;

		shape.SetAsBox(5.f, 2.5f, b2Vec2(x.a[0], x.a[1]), 0.0);
		pd.color = majo ? b2ParticleColor(255, 245, 179, 1) : b2ParticleColor(238, 16, 16, 1);
		auto group = particleSystem.CreateParticleGroup(pd);
	}

	rotation -= control->getMovement().x;
	rotation = fmaxf(rotation, -70);
	rotation = fminf(rotation, 70);

	++stepsSinceLastSpawn;
	return false;
}

void Tube::draw() const {
	jngl::pushMatrix();
	const auto transform = body->GetTransform();
	jngl::translate(jngl::Vec2(transform.p.x, transform.p.y));
	jngl::rotate(rotation);
	jngl::translate(jngl::Vec2(-transform.p.x, -transform.p.y));
	sprite.draw(jngl::modelview().translate(v2v(transform.p)).rotate(transform.q.GetAngle()));
	jngl::popMatrix();
}

bool Tube::isSqueezing() const {
	return squeezing;
}
