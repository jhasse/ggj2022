#version 100

uniform sampler2D tex;

varying mediump vec2 texCoord;

void main() {
	gl_FragColor = texture2D(tex, texCoord);
	lowp float threshold = 0.4;
	if (gl_FragColor.a < threshold) {
		gl_FragColor.a = 0.0;
	} else {
		gl_FragColor.a = 1.0;
	}
}
