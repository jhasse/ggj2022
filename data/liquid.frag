#version 300 es

uniform sampler2D tex;

in mediump vec2 texCoord;

out lowp vec4 outColor;

void main() {
	outColor = texture(tex, vec2(texCoord.x,
			                         texCoord.y));
	lowp float threshold = 0.4f;
	if (outColor.a < threshold) {
		outColor.a = 0.0f;
	} else {
		outColor.a = 1.0f;
	}
}
